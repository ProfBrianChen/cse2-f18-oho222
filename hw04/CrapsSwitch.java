//Olivia Ostrowski
//Fundementals of Programming CSE-002-310
//In this assignment I will be creating a game of craps using switch statements and asking the user which way they would like to get their numbers on the die
//This homework is due on Tuesday September 25th
import java.util.Scanner;
import java.util.Random; //using both methods
public class CrapsSwitch {
  public static void main (String [] args) {
    //create random generator for random numbers
    Random randGen = new Random();
    Scanner myScanner = new Scanner(System.in);
    //ask user to choose between randomly casting dice or state the two dice 
    System.out.println("Would you like randomly cast dice or state the two dice you want to evaluate?");
    System.out.println("Type 1 to randomly cast dice or type 2 to state dice."); //choose one or two for each choice
    
    int userChoice = myScanner.nextInt(); 
    //create if statement for if user chooses choice one
    if (userChoice == 1){
      int firstDie; //create integers for the first and second die
      int secondDie;
      
      firstDie = randGen.nextInt(6) + 1; //first die one to 6
      secondDie = randGen.nextInt(6) + 1; //seond die one to 6
      System.out.println("Your first die is " + firstDie); //print the random numbers for first and second die
      System.out.println("Your second die is " + secondDie);
       
      int sumofDie = (firstDie + secondDie); //create integer for the sum of the first and second die
       
      System.out.println("User choice is to randomly cast the dice."); //user chose to randomly generate
        switch (sumofDie) { //create switch statements with the sum of the two die being each case
          
          case 2:
            System.out.println("Your score is 2 and named Snake Eyes");
            break;
          
          case 3:
            System.out.println("Your score is 3 and named Ace Deuce"); 
            break;
       
          case 4:
            if(firstDie == 2 && secondDie == 2){ //create if statement within switch statement to decipher two die added up to make 4
              System.out.println("Your score is 4 and named Hard Four");
            }
            else{
              System.out.println("Your score is 4 and named Easy Four");
            }
            break;
          
          case 5:
            System.out.println("Your score is 5 and named Fever Five");
            break;
         
          case 6:
            if(firstDie == 3 && secondDie == 3){ //create if statement within switch statement to decipher two die added up to make 6
              System.out.println("Your score is 6 and named Hard Six");
            }
            else{
                System.out.println("Your score is 6 and named Easy Six");
              }
            break;
          
        case 7:
            System.out.println("your score is 7 and named Seven Out");
            break;
          
        case 8:
            if(firstDie == 4 && secondDie == 4){ //create if statement within switch statement to decipher two die added up to make 8
              System.out.println("Your score is 8 and named Hard Eight");
            }
              else{
                System.out.println("Your score is 8 and named Easy Eight"); 
              }
            break;
         
        case 9 :
            System.out.println("Your score is 9 and named Nine");
            break;
          
        case 10:
            if(firstDie== 5 && secondDie == 5){ //create if statement within switch statement to decipher two die added up to make 10
              System.out.println("Your score is 10 and named Hard Ten");
            }
              else{
                System.out.println("Your score is 10 and named Easy Ten");
              }
            break;
          
        case 11:
            System.out.println("Your score is 11 and named Yo-leven");
            break;
          
        case 12:
            System.out.println("Your score is 12 and named Boxcars");
            break;
           
         }

     }
      
     else if (userChoice == 2){ //create else if statement if the user chooses to choose their own values
      
       System.out.println("User choice is to state the two dice they want to evaluate."); //user chose choice 2
     
       System.out.println("Choose a number between 1 and 6"); //ask user to choose a number 1 to 6
      
       int firstDie = myScanner.nextInt(); 
       System.out.println("Choose another number between 1 and 6"); //ask user to choose another number from 1 to 6
       int secondDie = myScanner.nextInt();
       System.out.println("You chose " + firstDie + " and " + secondDie); //print out the two numbers
       int sumOfDie = (firstDie + secondDie); //create integer for the sum of first die and second die
        
       switch (sumOfDie) { //create switch statement 
          
         case 2:
            System.out.println("Your score is 2 and named Snake Eyes");
            break;
          
         case 3:
           System.out.println("Your score is 3 and named Ace Deuce");
           break;
          
         case 4:
           if(firstDie == 2 && secondDie == 2){ //create if statement within switch statement to decipher two die added up to make 4
             System.out.println("Your score is 4 and named Hard Four");
           }
           else{
             System.out.println("Your score is 4 and named Easy Four");
           }
             break;
          
         case 5:
            System.out.println("Your score is 5 and named Fever Five");
            break;
          
         case 6:
           if(firstDie == 3 && secondDie == 3){ //create if statement within switch statement to decipher two die added up to make 6
             System.out.println("Your score is 6 and named Hard Six");
             }
           else{
             System.out.println("Your score is 6 and named Easy Six");
             }
           break;
          
         case 7:
           System.out.println("your score is 7 and named Seven Out");
           break;
         
         case 8:
           if(firstDie == 4 && secondDie == 4){ //create if statement within switch statement to decipher two die added up to make 8
             System.out.println("Your score is 8 and named Hard Eight");
             }
           else{
             System.out.println("Your score is 8 and named Easy Eight"); 
             }
            break;
          
         case 9 :
           System.out.println("Your score is 9 and named Nine");
           break;
          
         case 10:
           if(firstDie== 5 && secondDie == 5){ //create if statement within switch statement to decipher two die added up to make 10
             System.out.println("Your score is 10 and named Hard Ten");
             }
           else{
             System.out.println("Your score is 10 and named Easy Ten");
           }
             break;
           
         case 11:
           System.out.println("Your score is 11 and named Yo-leven");
           break;
          
         case 12:
           System.out.println("Your score is 12 and named Boxcars");
           break;
           
       }
     }
   }
}