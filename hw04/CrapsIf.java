//Olivia Ostrowski
//Fundementals of Programming CSE-002-310
//In this assignment I will be creating a game of craps using if and else if statements statements and asking the user which way they would like to get their numbers on the die
//This homework is due on Tuesday September 25th
import java.util.Scanner;
import java.util.Random; //using both methods

  public class CrapsIf {
  
    public static void main (String [] args) {
   //create random generator for number
    Random randGen = new Random();
    Scanner myScanner = new Scanner(System.in);
    //ask user which choice they would like
    System.out.println("Would you like randomly cast dice or state the two dice you want to evaluate?");
    System.out.println("Type 1 to randomly cast dice or type 2 to state dice.");
    
    int userChoice = myScanner.nextInt();
    //create if statement if the user wants to randomly cast the dice
     if (userChoice == 1){
      int firstDie; //create variable for both die
      int secondDie;
      firstDie = randGen.nextInt(6) + 1; //first die random generator from one to 6
      secondDie = randGen.nextInt(6) + 1; //seond die random generator from one to 6
      System.out.println("Your first die is " + firstDie); //print out first die number
      System.out.println("Your second die is " + secondDie); //print out second die number
      System.out.println("User choice is to randomly cast the dice."); //chose number 1, so user randomly casts dice
       //create if statements for all the different possibilities and their names
       if(firstDie==1 && secondDie==1) {
          System.out.println("The score is 2 and named Snake Eyes"); //1 and 1 is snake eyes
      }
        
        else if(firstDie==1 && secondDie ==2 || firstDie == 2 && secondDie == 1){ //create both possibilites within one statement because they have same result
          System.out.println("The score is 3 and named Ace Deuce");
        }
        
       else if(firstDie == 1 && secondDie ==3 || firstDie == 3 && secondDie == 1){
            System.out.println("The score is 4 and named Easy Four");
        }
        
       else if(firstDie == 1 && secondDie == 4 || firstDie == 4 && secondDie == 1){
            System.out.println("The score is 5 and named Fever Five");
        }
        
       else if(firstDie ==1 && secondDie == 5 || firstDie == 5 && secondDie == 1){
            System.out.println("The score is 6 and named Easy Six");
        }
        
       else if(firstDie == 1 && secondDie == 6 || firstDie == 6 && secondDie == 1){
            System.out.println("Your score is 7 and named Seven Out");
        }
        
       else if(firstDie == 2 && secondDie == 2){
             System.out.println("Your score is 4 and named Hard Four");
        }
        
       else if(firstDie == 2 && secondDie == 3 || firstDie == 3 && secondDie == 2){
             System.out.println("Your score is 5 and named Fever Five");
        }
        
       else if(firstDie == 2 && secondDie == 4 || firstDie == 4 && secondDie == 2){
             System.out.println("Your score is 6 and named Easy Six");
        }
        
       else if(firstDie == 2 && secondDie == 5 || firstDie == 5 && secondDie == 2){
             System.out.println("Your score is 7 and named Seven Out");
        }
       
       else if(firstDie == 2 && secondDie ==6 || firstDie == 6 & secondDie == 2){
              System.out.println("Your score is 8 and named Easy Eight");
        }
        
       else if(firstDie == 3 && secondDie == 3){
              System.out.println("Your score is 6 and named Hard Six");
        }
        
       else if(firstDie == 4 && secondDie == 3 || firstDie == 3 && secondDie == 4){
              System.out.println("Your score is 7 and named Seven Out");
        }
        
       else if(firstDie == 4 && secondDie == 4){
              System.out.println("Your score is 8 and named Hard Eight");
        }
        
       else if(firstDie == 4 && secondDie == 5 || firstDie == 5 && secondDie == 4){
              System.out.println("Your score is 9 and named Nine");
        }
        
       else if(firstDie == 4 && secondDie == 6 || firstDie == 6 && secondDie == 4){
               System.out.println("Your score is 10 and named Easy Ten");
        }
        
       else if(firstDie == 5 && secondDie == 5){
               System.out.println("Your score is 10 and named Hard Ten");
        }
        
       else if(firstDie == 5 && secondDie == 6 || firstDie == 6 && secondDie == 5){
               System.out.println("Your score is 11 and named Yo-leven");
                                           }
        
       else if(firstDie == 6 && secondDie == 6){
                System.out.println("Your score is 12 and named Boxcars");
        }   
     }
     //create if statement if the user chooses the number two to type two dice
     else if (userChoice == 2){
      System.out.println("User choice is to state the dice."); //chose numner two
      System.out.println("Choose a number between 1 and 6"); //ask user to choose the number between one and six
      int firstDie = myScanner.nextInt(); //use scanner to ask for first die
      System.out.println("Choose another number between 1 and 6");
      int secondDie = myScanner.nextInt(); //use scanner to ask for second die
      System.out.println("You chose " + firstDie + " and " + secondDie); //print out numbers the user chose
    
      if(firstDie==1 && secondDie==1) { //if statement for all of the possiblities that can occur and the names for them
          System.out.println("The score is 2 and named Snake Eyes");
      }
        else if(firstDie==1 && secondDie ==2 || firstDie == 2 && secondDie == 1){
          System.out.println("The score is 3 and named Ace Deuce");
        }
        
       else if(firstDie == 1 && secondDie ==3 || firstDie == 3 && secondDie == 1){
            System.out.println("The score is 4 and named Easy Four");
        }
        
       else if(firstDie == 1 && secondDie == 4 || firstDie == 4 && secondDie == 1){
            System.out.println("The score is 5 and named Fever Five");
        }
        
       else if(firstDie ==1 && secondDie == 5 || firstDie == 5 && secondDie == 1){
            System.out.println("The score is 6 and named Easy Six");
        }
        
       else if(firstDie == 1 && secondDie == 6 || firstDie == 6 && secondDie == 1){
            System.out.println("Your score is 7 and named Seven Out");
        }
       
       else if(firstDie == 2 && secondDie == 2){
             System.out.println("Your score is 4 and named Hard Four");
        }
        
       else if(firstDie == 2 && secondDie == 3 || firstDie == 3 && secondDie == 2){
             System.out.println("Your score is 5 and named Fever Five");
        }
        
       else if(firstDie == 2 && secondDie == 4 || firstDie == 4 && secondDie == 2){
             System.out.println("Your score is 6 and named Easy Six");
        }
        
       else if(firstDie == 2 && secondDie == 5 || firstDie == 5 && secondDie == 2){
             System.out.println("Your score is 7 and named Seven Out");
        }
        
       else if(firstDie == 2 && secondDie ==6 || firstDie == 6 & secondDie == 2){
              System.out.println("Your score is 8 and named Easy Eight");
        }
        
       else if(firstDie == 3 && secondDie == 3){
              System.out.println("Your score is 6 and named Hard Six");
        }
        
       else if(firstDie == 4 && secondDie == 3 || firstDie == 3 && secondDie == 4){
              System.out.println("Your score is 7 and named Seven Out");
        }
       
       else if(firstDie == 4 && secondDie == 4){
              System.out.println("Your score is 8 and named Hard Eight");
        }
        
       else if(firstDie == 4 && secondDie == 5 || firstDie == 5 && secondDie == 4){
              System.out.println("Your score is 9 and named Nine");
        }
        
       else if(firstDie == 4 && secondDie == 6 || firstDie == 6 && secondDie == 4){
               System.out.println("Your score is 10 and named Easy Ten");
        }
        
       else if(firstDie == 5 && secondDie == 5){
               System.out.println("Your score is 10 and named Hard Ten");
        }
        
       else if(firstDie == 5 && secondDie == 6 || firstDie == 6 && secondDie == 5){
               System.out.println("Your score is 11 and named Yo-leven");
        }
        
       else if(firstDie == 6 && secondDie == 6){
                System.out.println("Your score is 12 and named Boxcars");
        }
     }
   }
}