//Olivia Ostrowski
//October 25, 2018
//Methods
import java.util.Random;
public class Methods{
    public static void main(String[] args){
        System.out.print(randomInt);
      }
    
    public static String Adjectives( ) {
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      switch(randomInt){
        case 1:
          return "funny";
        case 2:
          return "cute";
        case 3:
          return "large";
        case 4:
          return "imnmature";
        case 5:
          return "ambitious";
        case 6:
          return "brave";
        case 7:
          return "loyal";
        case 8:
          return "grateful";
        case 9:
          return "annoying";
        case 10:
          return "intellgient";
        default:
          return " ";
      }//end case
    }
    public static String SubjectNouns() {
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      switch(randomInt){
        case 1:
          return "Olivia";
        case 2:
          return "Abby";
        case 3:
          return "Maggie";
        case 4:
          return "Jenna";
        case 5:
          return "Kelly";
        case 6:
          return "Katie";
        case 7:
          return "Alexa";
        case 8:
          return "Lauren";
        case 9:
          return "Gwen";
        case 10:
          return "Sydney";
        default:
          return " ";
      }//end first method
    }
      
    
    
    public static String PastVerbs( ) {
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      switch(randomInt){
        case 1:
          return "ran";
        case 2:
          return "sat";
        case 3:
          return "ate";
        case 4:
          return "walked";
        case 5:
          return "held";
        case 6:
          return "jumped";
        case 7:
          return "laughed";
        case 8:
          return "smiled";
        case 9:
          return "enjoyed";
        case 10:
          return "typed";
        default:
          return " ";
      
      }
    }
    
    public static String ObjectNouns() {
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      switch(randomInt){
        case 1:
          return "the";
        case 2:
          return "it";
        case 3:
          return "dog";
        case 4:
          return "tree";
        case 5:
          return "those";
        case 6:
          return "these";
        case 7:
          return "thing";
        case 8:
          return "line";
        case 9:
          return "smile";
        case 10:
          return "sing";
        default:
          return " ";
      
    }
    }

    
  }//end code