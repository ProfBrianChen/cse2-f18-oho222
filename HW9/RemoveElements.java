//Olivia Ostrowski
//November 23, 2018
//This homework has us use different methods to create an array with random numbers, 
//delete an element based off of its position and remove specific integers 
//I used different methods with arrays, random generator, for loops, for each loops, and if statements

import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan = new Scanner(System.in);
    int num [] = new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer = "";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      if(index < 0 || index >9){
        System.out.println("Error, index out of bounds. Re-enter new integer:");
        index = scan.nextInt();
      }
     
      newArray1 = delete(num,index);
      String out1= "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2 = "The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
  	 
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
    }
    while(answer.equals("Y") || answer.equals("y"));
  }
 
  
  public static String listArray(int []num){
    String out = "{";
    for(int j = 0; j < num.length; j++){
      if(j > 0){
        out += ", ";
      } 
      out += num[j];
    }
    out += "} ";
    return out;
  }
  
  public static int [] randomInput(){
    Random rand = new Random();//create random
    int [] randArray = new int [10];
    for(int i = 0; i < randArray.length; i++){//only 10 numbers needed
      randArray[i] = (int)(Math.random()*9);//puts random numbers into array
    }
    int [] array = randArray;
    return array;
 }
  
  
  public static int [] delete(int [] list, int pos){
    int newArray [] = new int[list.length - 1];//make new array with the length one minus original
    for (int i = 0, j = 0; i < list.length; i++) {//for loop to find element at pos
      if (i == pos) { //if i does not = pos then it goes back to for loop and gets next number
        continue; 
      } 
      newArray[j++] = list[i]; 
    } 
    return newArray; 
  }//end delete method 

  
  public static int [] remove(int [] list, int target){
   int counter = 0;  //trying to find how many of the targets there are
    for (int num: list) { //for each executes on each element of array
      if (num == target) { 
        counter++;//add to the counter until each element is checked and the targets were added 
      }//end if 
    }//end for loop 
    if (counter == 0) { //if the number we want to remove is not in array then return orginal
      return list; 
    } 
    //newArray is list minus the length of the amount of targets
    int[] newArray = new int[list.length - counter]; //suptract the length by the amount of the same target numbers there are
    int x = 0; //x is index of new array
    for(int i : list){ //i is value in lisy now
      if (i != target){ //if i is not a target number then the new array replaces that spot with the previous number
        newArray[x] = i; //goes through every number until each target number is gone and the other numbers replace
        x++; 
      }
    }         
    //list = null;        
    return newArray;
  }//end remove method
   
}//end class



