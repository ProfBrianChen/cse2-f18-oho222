//Olivia Ostrowski
//November 23, 2018
//In this homework asks we have to check that the 15 user inputted integers are integers, in range, and sorted 
//then we find the key using binary search, scramble the original array, then find the key using linear search
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class CSE2Linear{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in);//scanner object
    int [] grade = new int [15];//create array for cse grades
    int counter = 0; //index of the amount of ints in the grade array
    
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");//ask user for 15 ints
    int num = 0;//each grade
    boolean correct= false;
    
    while(correct == false || counter <= 14){//while it is false or less than amount of elements
      correct = scan.hasNextInt(); //used to check if user input is an integer
      if (correct){
        num = scan.nextInt(); //user input= num
      } 
      else{ 
        System.out.println("Error, input an integer."); //error statement
        scan.next(); //deletes input because it is not needed
      }
      while (num < 0 || num > 100) {//checks for range
        System.out.println("Error, input a valid number within the range.");//error statement for out of range integers
        num = scan.nextInt(); //new number
      }
      if(counter == 0){//counter is amount of ints in array
        grade[counter++] = num;
      }
      else{
        for(int i = 1; i <= counter; i++){//for loop to make sure it is in ascending order
          if(grade[counter-i] > num){//checks if previous number was greater
            correct = false;
            System.out.println("Error, integer is not in order.");//error statement
            break;//end loop
          }
        }
        if(correct){
          grade[counter++] = num;
        }
      }
    }//end while loop checking for the correct input and creating array
    
    for(int i = 0; i < grade.length; i++){// for loop to print all members of array
      System.out.println( grade[i] + " ");
    }
    //call methods and print
    System.out.println("Scrambled array: " + Arrays.toString(grade));//print original array that user inputs
    System.out.println("Enter grade to be searched for:");//ask user for key for binary search
    int key = scan.nextInt();
    int find = binarySearch(grade, key);//call method to use binary search to find key
    int [] scramble = new int [15];//create new array to get scrambled array
    scramble = scrambleArray(grade);//call method to scramble original grade array
    System.out.println("Scrambled array: " + Arrays.toString(scramble));//print scrambled array
    System.out.println("Enter grade to be searched for:");//ask user for key for linear search
    key = scan.nextInt();
    linearSearch(grade, key);
  }//end main method
    
    public static int binarySearch(int[] grade, int key) {
      int low = 0;//create low place
      int high = grade.length-1;//top number is where the length - 1 because array place starts at 0
      
      while(high >= low){//as long as the highest place is bigger than lowest
        int mid = (low + high)/2;//find middle place by finding average of high and low
        if(key < grade[mid]){//if the key is less than the number in the middle
          high = mid -1;//then the high is now 1 lower than the middle because it searches for the lower half now
        }
        else if(key == grade[mid]){//if the key is equal to the middle then return that integer
          return mid;//best case
        }
        else{//or the low is now equal to the middle + 1 because it searches the second half
          low = mid + 1;  
        }
      }//end while loop for finding key
      
      System.out.println(key + " was not found in the list with 4 iterations");//if the key is not there 
      return -1;
    }//end binary search method
  
  
  
  public static int[] scrambleArray(int[] grade){
    Random rand = new Random();  // random object
    for (int i = 0; i < grade.length; i++) {//random generator only goes up to 15 or length of grade array
      int x = rand.nextInt(grade.length);//x is the new position in array the number will be in
      int temp = grade[i];//swap numbers 
      grade[i] = grade[x];//each time it iterates the spot will be given a random place
      grade[x] = temp;//swap
    }
    return grade;//return new array
	}//end scramble method
  
  
  
  public static int linearSearch(int[] grade, int key) { //method for linear search
    for (int j = 0; j < grade.length; j++) {//cant be longer than the length of array
      if (key == grade[j]) //if key is in the position of j then that is the key
        return j;
    }
    return -1;//if it is not there return -1
  }//end linear search method

}//end class
