//Olivia Ostrowski
//November 15, 2018
import java.util.*;
public class Arrays{
  public static void main(String [] args){
    
    Scanner scan = new Scanner(System.in);
    int [] array0 = {2, 3, 4, 5, 6, 7, 8, 9};//array of 8
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    inverter(array0);//calling all other methods
    print(array0);
    System.out.println(" ");
    array1 = inverter2(array0);
    print(array1);
    inverter2(array2);
    System.out.println(" ");
    
    int [] array3 = new int[array2.length];
    for(int i = 0; i < array2.length; i++){
      array2[i] = array2[i];
    }
    print(array3);//print array 3
  }//end main
 
  public static int [] copy(int [] list){//accepts an integer array 
    int [] array = new int[list.length];//new integer array that is the same length as the input
    for(int i = 0; i < list.length; i++){//for-loop to copy each member of the input to the same member in the new array
        array[i] = list[i]; //single iterator traverses both arrays
    }
    return array;//returns an integer array as output
  }//end copy method
 
  public static void inverter(int [] list){//reverses the order of an array
    int counter = list.length-1;//accepts an array of integers 
    int temp = 0;
    for (int j = 0; j < list.length / 2; j++) {
      temp = list[j];
      list[j] = list[counter -j];//swap first and last then second and second to last and so on
      list[counter - j] = temp;//modifies the memory pointed to by the array input
    }//returns void
  }//end inverter method
  
  public static int [] inverter2(int [] list){
    copy(list);//uses copy() to make a copy of the input array
    inverter(list);//copy of the code from inverter() to invert the members of the copy
    return list;//returns the copy as output
  }//end inverter2 method
   
  public static void print(int [] list){//accepts any integer array
    for(int i = 0; i < list.length; i++){// for loop to print all members of array
      System.out.print(list[i] + " ");
      //returns nothing
    }//end for loop
  }//end of print statement
  
  
}//end class
