//olivia ostrowski 
//October 7th, 2018
//this program will use if statements, while loops, scanner object, and random numbers to desipher the type of hand each hand will be
//it will then calculate the probability of the code

import java.util.Scanner;
public class Hw05{ 
  public static void main (String[] args){


Scanner myScanner = new Scanner(System.in);// scanner object
  System.out.println("Choose how many times to genterate hands.");
  System.out.print("User integer is: " );
  //number of hands 
  int numHands=0; 
  //number of four-of-a-kind
  int fourCounter=0; 
  //number of three-of-a-kind
  int threeCounter=0; 
  //number of two-of-a-kind
  int twoCounter=0; 
  //number of one of-a-kind
  int oneCounter=0; 
 //checks if user input is an integer using a boolean
  boolean correctinput= true;
  while(correctinput){
    //correct if the input is an integer
    boolean correct = myScanner.hasNextInt(); 
    if (correct){
      numHands = myScanner.nextInt(); //user input= numHands
      correctinput = false; //leaves while loop
    } 
    else{ 
      myScanner.next(); //deletes input because it is not needed
      System.out.println("Input an integer."); //asks user again
    }
  }
  //create five different random cards 
  int card1=(int)((Math.random()*52 +1)); 
  int card2=(int)((Math.random()*52 +1)); 
  int card3=(int)((Math.random()*52 +1));
  int card4=(int)((Math.random()*52 +1));
  int card5=(int)((Math.random()*52 +1)); 


//ensure that there are none of the same cards
  int i=0;
  while (i<= numHands){
    card1=(int)((Math.random()*52 +1)); 
    do{ 
      card2=(int)((Math.random()*52 +1)); 
    }//none of the cards cant equal eachother
    while(card2==card1);
    do{
    card3=(int)((Math.random()*52 +1));
    }
    while(card3==card2||card3==card1);
    do{
      card4=(int)((Math.random()*52 +1));
    }
    while(card4==card3||card4==card2||card4==card1); 
    do{
      card5=(int)((Math.random()*52 +1)); 
    }
    while(card5==card4||card5==card3||card5==card2||card5==card1);
    i++;
 

    //if statements to see which type of hand they recieve
    //if statement to figure out the four-of-a-kind hand
    if ((card1%13 == card2%13 && card2%13 == card3%13 && card3%13 == card4%13) 
          || 
        (card2%13 == card3%13 && card3%13 == card4%13 && card4%13 == card5%13) 
          ||
         (card3%13 == card4%13 && card4%13 == card5%13 && card5%13 == card1%13) 
          ||
         (card4%13 == card5%13 && card5%13 == card1%13 && card1%13 == card2%13) 
          ||
         (card5%13 == card1%13 && card1%13 == card2%13 && card2%13 == card3%13)){
      fourCounter++;//increment if true 
    }

    //if statement to figure out the three-of-a-kind hand
    if((card1%13 == card2%13 && card2%13 == card3%13) 
       ||
       (card2%13 == card3%13 && card3%13 == card4%13) 
       ||
       (card3%13== card4%13 && card4%13 == card5%13) 
       ||
       (card4%13 == card5%13 && card5%13 == card1%13) 
       ||
       (card5%13 == card1%13 && card1%13== card2%13)  
       ||
       (card1%13 == card3%13 && card3%13 == card3%13) 
       ||
       (card1%13 == card2%13 && card2%13 == card4%13) 
       ||
       (card2%13 == card3%13 && card3%13 == card5%13) 
       ||
       (card2%13 == card4%13 && card4%13 == card5%13) 
       ||
       (card1%13 == card3%13 && card3%13== card5%13)){
      
      threeCounter++;
    }

    //if statement to figure out a two pair hand
    if((card1%13 == card2%13 && card3%13 == card4%13)
           ||
       (card1%13 == card2%13 && card3%13 == card5%13)
           ||
       (card1%13 == card2%13 && card4%13 == card5%13)
           ||
       (card1%13 == card3%13 && card2%13 == card4%13)
           ||
       (card1%13 == card3%13 && card2%13 == card5%13)
           ||
       (card1%13 == card3%13 && card4%13 == card5%13)
           ||
       (card1%13 == card4%13 && card2%13 == card3%13)
           ||
       (card1%13 == card4%13 && card2%13 == card5%13)
           ||
       (card1%13 == card4%13 && card3%13 == card5%13)
           ||
       (card1%13 == card5%13 && card2%13 == card3%13)
           ||
       (card1%13 == card5%13 && card2%13 == card4%13)
           ||
       (card1%13 == card5%13 && card3%13 == card4%13)
           ||
       (card2%13 == card3%13 && card4%13 == card5%13)
           ||
       (card3%13 == card4%13 && card2%13 == card5%13)) {
      
      twoCounter++;
        }
        

    //if statement to figure out a one pair hand
    if((card1%13 == card2%13 && card3%13 != card4%13 && card4%13 != card5%13 && card3%13 != card5%13)
           ||
       (card1%13 == card3%13 && card2%13 != card4%13 && card4%13 != card5%13 && card2%13 != card5%13)
           ||
       (card1%13 == card4%13 && card2%13 != card3%13 && card3%13 != card5%13 && card2%13 != card5%13)
           ||
       (card1%13 == card5%13 && card2%13 != card3%13 && card3%13 != card4%13 && card2%13 != card4%13)
           ||
       (card2%13 == card3%13 && card1%13 != card4%13 && card4%13 != card5%13 && card1%13 != card5%13)
           ||
       (card2%13 == card4%13 && card1%13 != card3%13 && card3%13 != card5%13 && card1%13 != card5%13)
           ||
       (card2%13 == card5%13 && card1%13 != card3%13 && card3%13 != card4%13 && card1%13 != card4%13)
           ||
       (card3%13 == card4%13 && card1%13 != card2%13 && card2%13 != card5%13 && card1%13 != card5%13)
           ||
       (card3%13 == card5%13 && card1%13 != card2%13 && card2%13 != card4%13 && card1%13 != card4%13)
           ||
       (card4%13 == card5%13 && card1%13 != card2%13 && card2%13 != card3%13 && card1%13 != card3%13)){
      oneCounter++;
    }
    i++; 

  }
  //print amount of hands generated
  System.out.println("The number of hands generated is: " + numHands);
      
  //calculating probability of each type of hands
  //probability of a four of a kind hand to occur depending on the number of hands the user provides
  double fourProb = (double)fourCounter/(double)numHands;
  System.out.printf("The probability of four-of-a-kind is %.3f\n", fourProb);//use mod to make sure there are 3 decimals
  
  //probability of a three of a kind hand to occur depending on the number of hands the user provides
  double threeProb = (double)threeCounter/(double)numHands;
  System.out.printf("The probability of three-of-a-kind is %.3f\n", threeProb);  
  
  //probability of a two pair hand to occur depending on the number of hands the user provides
  double twoProb = (double)twoCounter/(double)numHands;
  System.out.printf("The probability of two-pair is %.3f\n", twoProb);
  
  //probability of a one pair hand to occur depending on the number of hands the user provides
  double oneProb = (double)oneCounter/(double)numHands;
  System.out.printf("The probability of one-pair is %.3f\n", oneProb);
       


   
  }  
} 
