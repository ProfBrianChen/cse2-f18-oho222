

public class Arithmetic{
  
  public static void main(String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    
    double totalCostOfPants = (numPants * pantsPrice); //total cost of pants
    double totalCostOfShirts = (numShirts * shirtPrice); //total cost of shirts
    double totalCostOfBelts = (numBelts * beltCost); //total cost of belts
    
    //sales tax on each pair of pants
    double salesTaxOnPants = (paSalesTax * totalCostOfPants);
    // sales tac on each shirt
    double salesTaxOnShirts = (paSalesTax * totalCostOfShirts);
    //sales tax on each belt
    double salesTaxOnBelts = (paSalesTax * totalCostOfBelts);
    
     //taking your answer, multiplying it by 100, converting to an int, and dividing the result by 100.0.
    salesTaxOnPants = salesTaxOnPants * 100;
    salesTaxOnPants = (int) salesTaxOnPants;
    salesTaxOnPants = salesTaxOnPants / 100;
    
    //eliminating extra digts in sales tax on shirts 
    salesTaxOnShirts = salesTaxOnShirts * 100;
    salesTaxOnShirts = (int) salesTaxOnShirts;
    salesTaxOnShirts = salesTaxOnShirts / 100;
    
    //eliminating extra digits in sales tax on belts
    salesTaxOnBelts = salesTaxOnBelts * 100;
    salesTaxOnBelts = (int) salesTaxOnBelts;
    salesTaxOnBelts = salesTaxOnBelts / 100;
   
    //total cost of purchase before tax
    double totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    //total sales tax
    double totalSalesTax = (salesTaxOnPants) + (salesTaxOnShirts) + (salesTaxOnBelts);
    
    //total of transaction including sales tax
    double transactionTotal = totalCostOfPurchase + totalSalesTax;
    
    System.out.println("The total cost of shirts is" + " " + totalCostOfShirts);
    System.out.println("The total cost of pants is" + " " + totalCostOfPants);
    System.out.println("The total cost of belts is" + " " +totalCostOfBelts);
    System.out.println("The sales tax on pants is" + " " +salesTaxOnPants);
    System.out.println("The sales tax on shirts is" + " " + salesTaxOnShirts);
    System.out.println("The sales tax on belts is" + " " + salesTaxOnBelts);
    System.out.println("The total cost of the purchase before tax is" + " " + totalCostOfPurchase);
    System.out.println("The total sales tax is" + " " + totalSalesTax);
    System.out.println("The total of the transaction with tax is" + " " + transactionTotal);
    
    
  }
  
}
    
    
    
    
    
    
    
    