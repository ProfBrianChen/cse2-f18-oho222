//Olivia Ostrowski
//November 8, 2018
// getting familiar with one-dimensional arrays; Write a program that randomly places integers between 0-99 
//into an array and then counts the number of occurrences of each integer
import java.util.*;
public class ArrayLab{
  public static void main(String [] args){
    
    int [] arrayOne = new int[100];//create array 1
    Random rand = new Random();//random generator
        for(int i = 0; i <  arrayOne.length; i++) {
            arrayOne[i] = (rand.nextInt(100));//make random numbers between 0 and 99
            System.out.println( + arrayOne[i] + "  ");
        }
    
    int [] arrayTwo = new int[100];//amount of elements in array 2
    for(int i = 0; i < 100; i++){
      int x = 0;
      for(int j = 0; j < arrayOne.length; j++){
        if( i == arrayOne[j]){//finds everytime it shows up
          x++;//add counter to array 2 at the number it was checking for
        }
      }
      arrayTwo[i] = x;
    }
    
    for(int k = 0; k < 100; k++){//printing everytime an index of array 2 does not equal 0
      if(arrayTwo[k] != 0){//in array 1 it shows up more times
        System.out.println(k + " " + "occurs" + " "+ arrayTwo[k] + " " + "times");//print statement
      }
    }
  }//end main method
}//end class
