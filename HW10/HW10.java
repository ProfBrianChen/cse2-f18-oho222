//Olivia Ostrowski
//December 1, 2018
//this homework uses multidimensional arrays and different methods to create a game of tic tac toe
import java.util.Scanner;
public class HW10 {
 
  public static void showGrid(char[][] grid) {//show grid method prints the grid everytime the grid changes, so when the players make a move
    for(int row = 0; row < grid.length; row++) {
      for (int col = 0; col < grid[row].length; col++){//nested for loop bc multidimensional array
        if (col == grid[row].length - 1){ 
          System.out.print(grid[row][col] + " " + " ");
        }
        else{ 
          System.out.print(grid[row][col] + " " + " ");
        }
      }
      System.out.println();
    }
  }
  
  public static boolean openSpot(char[][] grid){//checks if the spot is taken yet or not
    for (int row = 0; row< grid.length; row++) {
      for (int col = 0; col < grid[0].length; col++) {
        if (grid[row][col] != 'O' && grid[row][col] != 'X') {
          return true;//if it is not taken yet, we return true
        }
      }
    }
    return false;//if it is we return false
  }
 
  public static boolean win(char[][] grid){//calls all type of winning methods 
    return horoWin(grid) || vertWin(grid) || diagWin(grid);
  }

   
  private static boolean horoWin(char[][] grid) {//checks each row to see if there is a winner horozontally 
    for(int row = 0; row < grid.length; row++){
      if(done(grid[row]))
        return true;
    }
    return false;
  }


  private static boolean done(char[] line) {//checks if the user actully won by checking each line
    boolean foundWin = true;
    char first = ' ';
    for(char character: line) {//for each loop
      if(first == ' ')
        first = character;
      if ('O' != character && 'X' != character) {
        foundWin = false;//no one won yet in this case
        break;
      } 
      else if (first != character){
        foundWin = false;
        break;
      }
    }
    return foundWin;
  }

 
  private static boolean vertWin(char[][] grid) {//checks each column to see if they are the winner
    char[] column = null;
    for(int col = 0; col < grid[0].length; col++){
      column = new char[grid[0].length]; 
      for(int row = 0; row < column.length; row++){
        column[row] = grid[row][col];
      }
      if(done(column))//checks if there are x or os in each line
        return true;//returns true if they are winner
    }
    return false;//returns false if they are not
  }

  private static boolean diagWin(char[][] grid) {//checks each diagonal to see if they win
    int row = 0;
    int col = 0;
    int size;
    if(grid[0].length < grid.length){//if first length is less than full
      size = grid[0].length;//then the size of diagonal array is the first length
    }
    else{
      size = grid.length;
    }
    char[] diagonal = new char[size];//create new array as diagonal;
    while (row < grid[0].length && col < grid.length) {//works with row
      diagonal[col] = grid[row][col];
      row++;//increment both until we find the amount of each
      col++;
    }
    if (done(diagonal)) {
      return true;//is true because it is occupied on the diagonal
    }
    row = grid[0].length - 1;
    col = 0;
    diagonal = new char[size];
    while (row >=0 && col < grid.length) {//works with column
      diagonal[col] = grid[row][col];
      row--;
      col++;
    }
    return done(diagonal);//diagonal 
  }//end diagWin method

  

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int row;
    int column;
    char player = 'X';//player is x and o player
    char[][] grid = new char[3][3];//create array for 3x3 board
    char ch = '1';
    
    for (int rows = 0; rows < 3; rows++){//accesses whole multidimensional array
      for (int cols = 0; cols < 3; cols++) {
        grid[rows][cols] = ch++;
      }
    }
    
    showGrid(grid);//call method to print string
    while(!win(grid) == true){
      System.out.println("Enter a row and column (0, 1, or 2); for player " + player + ":");//first player foes
      row = scan.nextInt();
      column = scan.nextInt();
      
      while(row < 0 || row > 2 || column < 0 || column > 2){//if it is not in bounds rechoose two numbers
        System.out.println("Error, out of bounds. Enter new numbers: ");
        row = scan.nextInt();
        column = scan.nextInt();
      }

      //occupied
      while (grid[row][column] == 'X' || grid[row][column] == 'O') {//checks if it is already occupied by x or o
        System.out.println("This spot is occupied. Please try again");
        row = scan.nextInt();
        column = scan.nextInt();
      }
      
      //place the X
      grid[row][column] = player;
      showGrid(grid);//print grid again
      if (win(grid)){//if someone won
        System.out.println("Player " + player + " is the winner!");//then print the winner
      }

      
      if (player == 'O') {// swaps players after each go.
        player = 'X';
      }
      else {
        player = 'O';
          }
      if (win(grid) == false && !openSpot(grid)) {//neither player won and there are no more open spots
        System.out.println("The game is a draw. Please try again.");
      }
    }
  }
}
