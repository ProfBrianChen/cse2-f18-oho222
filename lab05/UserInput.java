//Olivia Ostrowski
//October 8th 2018
//this lab uses while loops and if statements to check if a number of inputs are correct
import java.util.Scanner;
public class UserInput {
  public static void main(String [] args) {
    Scanner scan = new Scanner(System.in);//scanner object declared
    
    //assign and input each variable needed
    int courseNum = 0; 
    String depName = " ";
    int numMeet = 0;
    int timeClass = 0;
    String teacherName = " ";
    int studentsNum = 0;
    boolean correct = false;
   
   //make sure it works
   System.out.print("Write course number: ");
    while(!correct){
      correct = scan.hasNextInt();//checking using this statement
      if (correct){
        courseNum = scan.nextInt();
      }
      else{
        System.out.println("Enter new input due to error.");
        scan.next();//deletes unneccesary information 
      }
    }
    System.out.println("The course number is: " + courseNum);//check if this works
    
    //next step is the department name
    System.out.print("Write the department name: ");
    correct = scan.hasNext();
    while(!correct){
      scan.next();
      correct = scan.hasNext();
    }
    
    depName = scan.next();
    System.out.println("The department name is: " + depName);
    
    //next step is the number of times it meets in a week
    System.out.print("Write the amount of times the course meets in a week: ");
    correct = false;//checking
    while(!correct){
      correct = scan.hasNextInt();
      if(correct){
        numMeet = scan.nextInt();
      }
      else{
        System.out.println("Enter new input due to error.");
        scan.next();//deletes other input
      }
    }
    System.out.println("The amount of times the course meets in a week is: " + numMeet );
    //print the amount the course meets
    
    //next step to check is the time of the class
    System.out.print(" Write the time the course is at: ");
    correct = false;
    while(!correct){
      correct = scan.hasNextInt();
      if(correct){
        timeClass = scan.nextInt();
      }
      else{
        System.out.println("Enter new input due to error.");
        scan.next();
      }
    }
    System.out.println("The time of this course is: " + timeClass);
    
    //the next step is to check the instructor of this course
    System.out.print("Write the name of the instructor: ");
    correct = scan.hasNext();
    while(!correct){
      scan.next();
      correct = scan.hasNext();
    }
    
    teacherName = scan.next();
    System.out.println("The name of the instructor is: " + teacherName );
    
   //next step is the number of students in the course
    System.out.print("Write the number of students: ");
    correct = false;
    while(!correct){
      correct = scan.hasNextInt();
      if(correct){
        studentsNum = scan.nextInt();
      }
      else{
        System.out.println("Enter new input due to error");
        scan.next();
      }
    }
    System.out.println("The number of students in the class is: " + studentsNum);
    
  
 
    
    
  }
}
 
    
    