//Olivia Ostrowski
//October 19, 2018
//This homework involves scanner, while loops, for loops, and if else statements to print a 
//star pattern of a square with an X embedded inside of it 
//The size of the square depends on the user's input to desipher the side lengths
import java.util.Scanner;
public class EncryptedX {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);//state scanner object
    System.out.println("Input an integer between 1 and 100:");//ask user for input
    
    int sideNum = 0;
    boolean correct= false;
    
    while(!correct){//while loop to check if the input is integer
      correct = scan.hasNextInt(); //used to check if user input is an integer
      
      if (correct){
        sideNum = scan.nextInt(); //user input= sideNum
      } 
      else{ 
        System.out.println("Error, input an integer."); //asks user again
        scan.next(); //deletes input because it is not needed
      }
    }//end of while loop to ensure user input is an integer
    
    
    while (sideNum <0 || sideNum > 100) {
      System.out.println("Error, input a valid number within the range.");//asks user to re enter an integer
      sideNum = scan.nextInt(); 
    }//end of while loop checking if it is between 1 and 100
    
    //for loop to create the shape of pattern
    for(int i = 0; i <= sideNum; i++){//outer loop refers to rows 
      for(int j = 0; j<= sideNum; j++){//inner loop refers to columns
            
        if(j == i || (j==(sideNum-i))){
          System.out.print(" ");//creating the X within the stars
        }
        else
          System.out.print("* ");//if that is not the case print the stars
      }//end of inner loop
      System.out.println(" ");
    }//end of outer loop

  }//end class
}//end code
