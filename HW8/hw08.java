//Olivia Ostrowski
//November 9, 2018
//practice in manipulating arrays and in writing methods that have array parameters.

import java.util.*;
public class hw08{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i = 0; i < 52; i++){ 
      cards[i] = rankNames[i%13] + suitNames[i/13]; 
      System.out.print(cards[i] + " ");
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
      hand = getHand(cards,index,numCards);
      printArray(getHand(cards,index,numCards));
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt();
    }  
  } //end main method
 

public static void shuffle(String [] list){
  
  for(int j = 0; j < 60; j++){//choose > 50 times since you want the deck of 52 cards well shuffled.
   Random rand = new Random();//randomize an index number of list
    int x = rand.nextInt(51);//52 numbers
    String temp =  list[0];//swaps the element at that index with the first element 
    list[0] = list[x];
    list[x] = temp;
  }
}//end method 

public static String [] getHand(String [] list, int index, int numCards){
  String [] listTwo = new String[numCards];
  if(numCards<= index){
    for(int k = 0; k < numCards; k++){
      listTwo[k] = list[index - k];
    }
  }
  if(numCards>52){
  System.out.println(Arrays.toString(list));
  //create new deck
  }
  return listTwo;//returns an array that holds the number of cards specified in numCards
}//end method

public static String printArray(String [] list){//takes an array of Strings and prints out each element, separated by a space 
  String a = " "; 
  for(int i = 0; i < list.length; i++){//prints each individual element
    a = a + list[i] + " ";
  }
  return " ";
  
}//end method
}//end class