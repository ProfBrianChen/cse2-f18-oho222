import java.util.Scanner;
public class WordTools {
   //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String inputString = " ";
    char menuInput = ' ';
    String find = " ";
    
    System.out.println("Enter a sample text:");
     inputString = scan.nextLine();
    System.out.println("You entered: " + inputString);
    
    // continue to call printMenu() each time the user enters an incorrect input, 
    //after each time the user enters an acceptable input, 
    //until the user enters q to Quit
    while (menuInput != 'q'){//while it is not quitting with q
      menuInput = printMenu(scan);
      
      if (menuInput == 'c') {//all options of menu
        System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(inputString));
      }//if user chooses c
     
      else if (menuInput == 'w') {
        System.out.println("Number of words: " + getNumOfWords(inputString));
      }
      
      else if (menuInput == 'f'){
        System.out.println("Enter a word or phrase to be found:");//extra for findText method because user has to specify what they want to find
        find = scan.nextLine();
        System.out.println("\"" + find + "\" instances: " + findText(find, inputString));
      }
     
      else if (menuInput == 'r') {
        System.out.println("Edited text: " + replaceExclamation(inputString));
      }
      
      else if (menuInput == 's') {
        System.out.println("Edited text: " + shortenSpace(inputString));
      }
      
      else{
        System.out.println("Error, choose an option given");
      }
      
      return;
    }
  }// end main
  
  
  
  public static int getNumOfWords(final String inputString) {//method to find number of words in string
    int wordNum = 0;
    int i = 0;
    for (i = 1; i < inputString.length(); ++i) {//inputString.length() returns the length of the string.
      if ( (Character.isWhitespace(inputString.charAt(i)) == true) && (Character.isWhitespace(inputString.charAt(i - 1))) == false) {//error if you run charAt(i) for some i greater than inputString.length()-1
        //inputString.charAt(i)returns the char at position i in the string, where position 0 is the first character.
        ++wordNum;
      }//end if statement
    }//end for loop
    if (Character.isWhitespace(inputString.charAt(i - 1)) == false) {
      ++wordNum;
    }//end 2nd if statement
    return wordNum;
  }//end getNumOfWords method
 
  
  public static int getNumOfNonWSCharacters(final String inputString) {//returns the number of characters in the String, excluding all whitespace
    int wordNum = 0; //string as a parameter 
    int i = 0;
    for(i=0; i < inputString.length(); i++){//inputString.length() returns the length of the string
      if (inputString.charAt(i) != ' '){
        ++wordNum;
      }//end if statement
    }//end for loop
    return wordNum;
  }//end getNumOfNonWSCharacters method


  public static int findText(final String find, String inputString) {//two strings as parameters; tect to be found and the string the user inputted
    int wordNum = 0;
    int locate = 0;
    do {// quits when locate equals to -1
      locate = inputString.indexOf(find);
      if (locate == -1) {
        return wordNum;
      }//end if statement
      wordNum++;
      inputString = inputString.substring(locate + 1, inputString.length());
    }//
    while(locate >= 0); // End do-while
    return wordNum;//returns the number of instances a word or phrase is found in the string
  }//end method findText

  
  public static String replaceExclamation(String inputString) {//sting perameter
    return inputString.replace('!', '.');//replaces '!' character in the string with a '.' 
  }//end replaceExclamation method

  
  public static String shortenSpace(String inputString) {//string perameter
      while (inputString.indexOf(" ") != -1) {//find if there are double spaces
      inputString = inputString.replaceAll(" ", " ");//replace two spaces with one
    }//end while loop
    return inputString;//replaces all sequences of 2 or more spaces with a single space
  }//end shortenSpace method

  
  public static char printMenu(Scanner scan) {
    char menuChoice = ' ';
    //print menu
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.print("\n");

    while (menuChoice != 'c' && menuChoice != 'w' && menuChoice != 'f' && menuChoice != 'r' && menuChoice != 's' && menuChoice != 'o' && menuChoice != 'q') {
      System.out.println( "Choose an option:");
      menuChoice = scan.nextLine().charAt(0);
    }//end while loop
    return menuChoice;//return character 
  }//end printMenu method
  
 
}// end class