//Olivia Ostrowski
//10/11/18
import java.util.Scanner;
public class PatternC {
  public static void main(String [] args){
    Scanner scan = new Scanner(System.in);
    int numRows= 0;
    boolean correct = false;
    System.out.println("Enter integer between 1-10.");
    System.out.print("User integer is: ");
    
    while(!correct){
      correct = scan.hasNextInt();//checking using hasNextInt statement
      if (correct){
         numRows = scan.nextInt();
      }
      else{
        System.out.println("Enter new input due to error.");
        scan.next();//deletes unneccesary information 
      }
    }//end of while loop 1
    
    while(numRows > 10 || numRows< 1){
      System.out.println("Error input new integer");
      numRows = scan.nextInt();
    }
    System.out.println("The integer is: " + numRows);//Print the integer the user chose
    
    //for loop to desipher the type of pattern that will occur with the certain number of rows
    for (int i = 1; i <= numRows; i++) {
      int k=i;
      for (int j = 1; j <= numRows; j++) {
        System.out.print((j<= numRows-i  ? " " : k--));                                                 
      }
      System.out.println();
   
    }//end for loop

  } //end class
}//end code
  