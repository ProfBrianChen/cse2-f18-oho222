import java.util.Random;

// This lab session is an exercise in using if statements, switch statements  and in using Math.random(), a random number generator
public class CardGenerator {
    // main method required for every Java program
   	public static void main(String[] args) {
     
      int randomNum = (int)(Math.random()*51); // math random from 1 to 52 inclusive
     
      String identityString = "randomNum"; 
     
      int randomNum1 = randomNum%13; //mod 13 because there are 13 cards per suit
      
      //switch statement to assign card identity 
      //depending on the random number mod 13 the case number will provide the card identity
      switch (randomNum1) { 
      
      case 1: identityString = "Ace";
      break;
      case 2: identityString = "2";
      break;
      case 3: identityString = "3";
      break;
      case 4: identityString = "4";
      break;
      case 5: identityString = "5";
      break;
      case 6: identityString = "6";
      break;
      case 7: identityString = "7";
      break;
      case 8: identityString = "8";
      break;
      case 9: identityString = "9";
      break;
      case 10: identityString = "10";
      break;
      case 11: identityString = "Jack";
      break; 
      case 12: identityString = "Queen";
      break;
      case 13: identityString = "King";
      break;
     }
      
    
    int myNum = randomNum;
   
    //if statements to find specific suit name
      //if the number is greater than or equal to 1 and less than or equal to 13 it is a diamond
    if (myNum >=1 && myNum <= 13) {
    System.out.println("You picked the " + identityString + " of Diamonds");
    }
      
    //if the number is greater than or equal to 14 and less than or equal to 26 it is a club
    else if (myNum >=14 && myNum <= 26) {
    System.out.println("You picked " + identityString + " of Clubs");
    }
     
     //if the number is greater than or equal to 27 and less than or equal to 39 it is a heart
    else if (myNum >=27 && myNum <= 39) {
    System.out.println("You picked " + identityString + " of Hearts");
    }
    
     //if the number is greater than or equal to 40 and less than or equal to 52 it is a spade
    else if (myNum >=40 && myNum <= 52) {
    System.out.println("You picked " + identityString + " of Spades");
    }
    
      
     
     
    
      
    }
}