import java.util.Scanner;

public class Pyramid {
  public static void main(String[] agrs) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("What is the length of the base of the pyramid");
    double length = myScanner.nextDouble();
    
    System.out.println("What is the height of the base of the pyramid");
    double height = myScanner.nextDouble();
   
    double volume = (length * length * height) /3;
    System.out.println("The volume of the pyramid is: " + volume);
  }
}