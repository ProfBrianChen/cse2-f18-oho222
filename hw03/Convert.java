import java.util.Scanner;

public class Convert {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
      
    //Convert the quantity of rain into cubic miles
    //asks user for doubles that represent number of acres of land affected by hurricabe precipitation
    System.out.println("What is the number of acres of land that were in the area affected by the hurricane?");
    double numAcres = myScanner.nextDouble();
    
    //asks user for doubles that represent how many inches of rain were dropped on average.
    System.out.println("What is the amount of inches of rainfall in the affected area?");
    double inchesOfRain = myScanner.nextDouble();
    
    System.out.println(numAcres);
    System.out.println(inchesOfRain);
    
    //converts acre-inches(* acre times inches) to number of gallons of rain
    double gallons = numAcres * inchesOfRain *  27154.285990761;
    //converts number of gallons of rain to cubic miles
    double cubicMiles = gallons * 9.08169e-13;
    
    System.out.println();
    System.out.println("The number of acres of land that were affected by the hurricane is " + numAcres + " acres.");
    System.out.println("The amount of inches of rainfall in the affected area was " + inchesOfRain + " inches.");
    System.out.println("The final answer is " + cubicMiles + " miles cubed");
  }
}